import React from "react";

import QueryMenu from "../../components/QueryMenu";

import { Link } from "react-router-dom";

import papikImage from "../../img/papik-pro-p-bumaga-vektornii-risunok-2.png";
import topImage from "../../img/top-fon-com-p-knizhki-dlya-prezentatsii-bez-fona-140.png";

import main from "./MainPage.module.scss";

function MainPage() {
    return (
        <div className={main.mainPage}>
            {/* <QueryMenu/>                                     */}
            <div className={main.item_1}>
                <img src={papikImage} alt="papik" className={main.image} /><p className={main.imageText}>Заявки</p>
                <img src={topImage} alt="top" className={main.image} /><p className={main.imageText}>База знаний</p>
            </div>

            <div className={main.item_2}>
                <QueryMenu />
            </div>
            <div className={main.item_3}>
                <button className={main.button}>Создать заявку</button>
                {/* <h3 className={main.topic}>Тема</h3>
            <hr className={main.line} />
            <p className={main.listItems}><Link to="/ticket">Список всех заявок</Link></p>
            <hr className={main.line} /> */}
                <table className={main.topic}>
                    <thead>
                        <tr>
                            <th className={main.theme}>Тема</th>
                            <th className={main.request}>№ заявки</th>
                            <th className={main.applicant}>Заявитель</th>
                            <th className={main.executor}>Исполнитель</th>
                            <th className={main.status}>Статус</th>
                            <th className={main.priority}>Приоритет</th>
                            <th className={main.activity}>Активность</th>
                        </tr>
                    </thead>                    
                    <tbody>
                        <tr>
                            <td className={main.listItems}><Link to="/ticket">Список всех заявок</Link></td>
                            <td className={main.listItems}><Link to="/ticket">ID-123</Link></td>
                            <td>Seric0</td>
                            <td>Специалист 3 линии</td>
                            <td>Открыта</td>
                            <td>Низкий</td>
                            <td>2 дня</td>
                        </tr>                        
                    </tbody>                    
                </table>
            </div>                        
        </div>
    );
}

export default MainPage;