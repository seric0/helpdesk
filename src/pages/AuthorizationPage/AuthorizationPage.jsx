import React, { useState } from "react";

import { Link } from "react-router-dom";
// import { useDispatch, useSelector } from "react-redux";

// import { authUser } from "../../store/userSlice";

import authorization from "./AuthorizationPage.module.scss";

function AuthorizationPage() {
  // const dispatch = useDispatch();
  // const { isLoading } = useSelector((state) => state.user);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();

    // dispatch(signIn({email, password}))
    // dispatch(authUser({ email, password }));
  };

  return (
    <div className={authorization.authorizationPage}>
      <div className={authorization.container}>
        <form className={authorization.form} onSubmit={onSubmit}>
          <div className={authorization.inputs}>
            <label className={authorization.label}>
              Email адрес
              <input
                type="text"
                className={authorization.input}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </label>
            <label className={authorization.label}>
              Пароль
              <input
                type="password"
                className={authorization.input}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </label>
          </div>
          <button type="submit" className={authorization.button}>
            Авторизация          
          </button>
          <div className={authorization.registrText}>
          <Link to={`/registr/`}>Регистрация</Link>
          </div>
        </form>
      </div>
    </div>
  );
}

export default AuthorizationPage;
