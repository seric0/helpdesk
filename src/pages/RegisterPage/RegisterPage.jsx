import React, { useState } from "react";

import { Link } from "react-router-dom";
// import { useDispatch } from "react-redux";

// import { signIn } from "../../store/userSlice";

import register from "./RegisterPage.module.scss";

function RegisterPage() {
  // const dispatch = useDispatch();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();

    // dispatch(signIn({email, password}))
  };

  return (
    <div className={register.authorizationPage}>
      <div className={register.container}>
        <form className={register.form} onSubmit={onSubmit}>
          <div className={register.inputs}>
            <label className={register.label}>
              Email address
              <input
                type="text"
                className={register.input}
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </label>
            <label className={register.label}>
              Password
              <input
                type="password"
                className={register.input}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </label>
            <label className={register.label}>
             Confirm
              <input
                type="password"
                className={register.input}
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </label>                        
          </div>
          <button type="submit" className={register.button}>
           Register
          </button>
          <div className={register.registrText}>
          <Link to={`/`}>Авторизация</Link>
          </div>
        </form>
      </div>
    </div>
  );
}

export default RegisterPage;
