import React from "react";

import QueryMenu from "../../components/QueryMenu";
import TicketRightMenu from "../../components/TicketRightMenu";

import papikImage from "../../img/papik-pro-p-bumaga-vektornii-risunok-2.png";
import topImage from "../../img/top-fon-com-p-knizhki-dlya-prezentatsii-bez-fona-140.png";

import ticket from "./TicketPage.module.scss";

function TicketPage() {
    return (
        <div className={ticket.mainPage}>                                             
            {/* <QueryMenu/>                                     */}
            <div className={ticket.item_1}>		    
            <img src={papikImage} alt="papik" className={ticket.image}/><p className={ticket.imageText}>Заявки</p>			
            <img src={topImage} alt="top" className={ticket.image}/><p className={ticket.imageText}>База знаний</p>		
        </div>
         
        <div className={ticket.item_2}>		    
        <QueryMenu/>
        </div>
        <div className={ticket.item_3}>
            <button className={ticket.button}>Создать заявку</button>
            <p className={ticket.topicRequest}>ID-123 от 29.04.2023 12:10</p>
            <h3 className={ticket.topic}>Тема</h3>
            <hr className={ticket.line} />            
        <div>
        <p className={ticket.listItems}>Список всех заявок.</p>
        <p className={ticket.listItems}>Список всех заявок.</p>            
        </div>
        </div>
        <div className={ticket.item_4}>
        <TicketRightMenu />        
        </div>
        </div>
    );
}

export default TicketPage;