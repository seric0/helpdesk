import React from "react";

import {BrowserRouter, Routes, Route} from "react-router-dom";

import AuthorizationPage from "./pages/AuthorizationPage";
import RegisterPage from "./pages/RegisterPage";
import MainPage from "./pages/MainPage";
import TicketPage from "./pages/TicketPage";

function Router() {
  return (    
    <BrowserRouter>
    <Routes>
    {/* <Route path="/" element={<HomePage />} /> */}
    <Route path="/" element={<AuthorizationPage />} />
    <Route path="/registr" element={<RegisterPage />} />
    <Route path="/main" element={<MainPage />} />
    <Route path="/ticket" element={<TicketPage />} />
    {/* <Route path="/news" element={<NewsPage />} />
    <Route path="/news/:id" element={<NewsCardPage />} /> */}
    <Route path="*" element={<div>404</div>} />
    </Routes>
    </BrowserRouter>
  );
}

export default Router;