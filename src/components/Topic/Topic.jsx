import React from "react";

import { Link } from "react-router-dom";

import topic from "./Topic.module.scss";

function Topic() {
    return (
        <div>
            <table className={topic.topic}>
                    <thead>
                        <tr>
                            <th className={topic.theme}>Тема</th>
                            <th className={topic.request}>№ заявки</th>
                            <th className={topic.applicant}>Заявитель</th>
                            <th className={topic.executor}>Исполнитель</th>
                            <th className={topic.status}>Статус</th>
                            <th className={topic.priority}>Приоритет</th>
                            <th className={topic.activity}>Активность</th>
                        </tr>
                    </thead>                    
                    <tbody>
                        <tr>
                            <td className={topic.listItems}><Link to="/ticket">Список всех заявок</Link></td>
                            <td className={topic.listItems}><Link to="/ticket">ID-123</Link></td>
                            <td>Seric0</td>
                            <td>Специалист 3 линии</td>
                            <td>Открыта</td>
                            <td>Низкий</td>
                            <td>2 дня</td>
                        </tr>                        
                    </tbody>                    
                </table>
        </div>
    );
}

export default Topic;