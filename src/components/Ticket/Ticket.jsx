import React from "react";

import ticket from "./Ticket.module.scss";

function Ticket() {
    return (
        <div>
            <p className={ticket.listItems}>Список всех заявок.</p>
        </div>
    );
}

export default Ticket;