import React from "react";

import querymenu from "./QueryMenu.module.scss";

function QueryMenu() {
    const open = 1;
    const waiting = null;
    const working = 2;
    const executed = 10;
    return (
        // <div className={querymenu.mainmenu}>
        // <ul className={querymenu.menu}>
        //     <li className={querymenu.menuelem}>Открыта<div className={querymenu.menucol}>{n}</div></li>
        //     <li className={querymenu.menuelem}>Ожидает ответа<div className={querymenu.menucol}>{n}</div></li>
        //     <li className={querymenu.menuelem}>В работе<div className={querymenu.menucol}>{n}</div></li>
        //     <li className={querymenu.menuelem}>Исполнена<div className={querymenu.menucol}>{n}</div></li>
        // </ul>
        // </div>

        <div className={querymenu.mainmenu}>
            <div className={querymenu.menu}>
                <div className={querymenu.menuelem}>Открыта</div>
                <div className={querymenu.menucol}>{open}</div>
                <div className={querymenu.menuelem}>Ожидает ответа</div>
                <div className={querymenu.menucol}>{waiting}</div>
                <div className={querymenu.menuelem}>В работе</div>
                <div className={querymenu.menucol}>{working}</div>
                <div className={querymenu.menuelem}>Исполнена</div>
                <div className={querymenu.menucol}>{executed}</div>
            </div>
        </div>
    );
}

export default QueryMenu;