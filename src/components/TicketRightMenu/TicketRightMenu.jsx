import React from "react";

import ticketright from "./TicketRightMenu.module.scss";

function TicketRightMenu() {
    const one_line = '1 линия поддержки';    
    return (
        <div className={ticketright.mainmenu}>
            <div className={ticketright.menu}>
                <div className={ticketright.menuelem}>Департамент</div>                
                <div><select id="support" name="support">
                    <option value="1 линия поддержки">{one_line}</option>
                    <option value="2 линия поддержки">2 линия поддержки</option>
                    <option value="3 линия поддержки">3 линия поддержки</option>
                    <option value="4 линия поддержки">4 линия поддержки</option>
                </select></div>
                <div className={ticketright.menuelem}>Исполнитель</div> 
                <div><select id="support" name="support">
                    <option value="Денис">Денис</option>
                    <option value="Серик">Серик</option>                    
                </select></div>               
                <div className={ticketright.menuelem}>Статус</div> 
                <div><select id="support" name="support">
                    <option value="Открыта">Открыта</option>
                    <option value="Ожидает ответа от пользователя">Ожидает ответа от пользователя</option>                    
                    <option value="В работе">В работе</option>
                    <option value="Исполнена">Исполнена</option>
                </select></div>               
                <div className={ticketright.menuelem}>№ кабинета</div>
                <div>< input type="text" /></div>
            </div>
        </div>
    );
}

export default TicketRightMenu;