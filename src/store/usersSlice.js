import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const endpoint = process.env.REACT_APP_ENDPOINT || "";

export const getUsers = createAsyncThunk(
    "users/getUsers",
    async ({ limit, page, isExpanded }, { rejectWithValue }) => {
      try {
        let queryParams = "";        
        if (limit) {
          if (queryParams) queryParams += "&";
          queryParams += `_limit=${limit}`;
        }
        if (page) {
          if (queryParams) queryParams += "&";
          queryParams += `_page=${page}`;
        }
        if (isExpanded) {
          if (queryParams) queryParams += "&";
          queryParams += `_expand=user`;
        }
        
        const response = await fetch(
          `${endpoint}/posts${queryParams ? `?${queryParams}` : ""}`
        );
  
        if (!response.ok) {
          throw new Error("Server error");
        }
  
        const data = await response.json();
  
        return data;
      } catch (error) {
        return rejectWithValue(error.message);
      }
    }
  );

const usersSlice = createSlice({
  name: "users",
  initialState: { users: [] },
  reducers: {        
  },
  extraReducers: {
    [getUsers.pending]: (state) => {
      state.isLoading = true;
    },
    [getUsers.rejected]: (state, action) => {
      state.isLoading = false;
      state.isError = true;
      state.error = action.payload;
    },
    [getUsers.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.isError = false;
      state.error = "";
      state.users = action.payload;
    },    
  },
});

export const { logOut } = usersSlice.actions;

export default usersSlice.reducer;
